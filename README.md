J'ai décidé pour ces 3 étapes de faire évoluer ma structure sans la dupliquer, pour tester les étapes j'ai fait 3 commits correspondant à chaque étape en espérant que c'était la bonne chose à faire.

Les étapes se trouvent dans src/main/java/backend, la seule library que j'ai utilisé est Gson pour la lecture et l'écriture des fichiers JSON (hormis SpringBoot).

Je vous remercie d'avance pour la correction de ce test en espérant avoir été à la hauteur de vos attentes.

PS : Lire le fichier README.md dans le dossier src/main/java/backend

