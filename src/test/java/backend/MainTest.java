package backend;

import backend.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    BaseModel baseModel = null;

    @BeforeEach
    public void init() {
        try {
            baseModel = Main.parseJson();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testDistributions() {
        try {

            int walletId = baseModel.getWallets().stream().filter(v -> "FOOD".equals(v.getType())).mapToInt(Wallet::getId).findFirst().getAsInt();
            assertEquals(2, walletId);

            int amount = 100;
            Company companySelected = baseModel.getCompanies().get(0);
            User userSelected = baseModel.getUsers().get(0);

            assertNotNull(companySelected);
            assertNotNull(userSelected);

            Distribution distribution = Main.distributionFromCompany(baseModel, companySelected, userSelected, amount, walletId);

            assertNotNull(distribution);
            assertNotNull(baseModel.getDistributions());
            assertEquals(amount, baseModel.getDistributions().get(0).getAmount());
            assertEquals("2022-02-28", baseModel.getDistributions().get(0).getEnd());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testCalculateBalanceUser() {
        try {

            int walletId = baseModel.getWallets().stream().filter(v -> "FOOD".equals(v.getType())).mapToInt(Wallet::getId).findFirst().getAsInt();
            assertEquals(2, walletId);

            int amount = 100;
            User userSelected = baseModel.getUsers().get(0);

            int prevAmount = 0;

            for (Balance balance : userSelected.getBalance()) {
                if (balance.getWalletId() == walletId) {
                    prevAmount = balance.getAmount();
                }
            }

            assertNotNull(userSelected);

            Main.calculateBalanceUser(baseModel, userSelected, amount, walletId);
            for (User userLocal : baseModel.getUsers()) {

                if (userLocal.getId() == userSelected.getId()) {

                    for (Balance balance : userLocal.getBalance()) {
                        if (balance.getWalletId() == walletId) {
                            assertEquals(balance.getAmount(), amount + prevAmount);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}