package backend;

import backend.model.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class Main extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Main.class);
    }

    /**
     * function main to start server spring boot
     * @param args
     */
    public static void main (String[] args) {
        SpringApplication.run(Main.class, args);
    }

    /**
     * This function allowing the company to distribute meal vouchers and gift cards.
     * @param baseModel
     * @param company
     * @param user
     * @param amount
     * @param walletId
     * @throws Exception
     */
    public static Distribution distributionFromCompany(BaseModel baseModel, Company company, User user, int amount, int walletId) throws Exception {

        // check if the company balance allows it
        if (company.getBalance() < amount)
            throw new Exception("Balance of company is insufficient");

        // creation new distrubition
        Distribution distribution = new Distribution(baseModel.getDistributions().size() + 1, amount, company.getId(), user.getId(), walletId);
        baseModel.getDistributions().add(distribution);

        // update the company balance
        company.setBalance(company.getBalance() - amount);

        // update the user balance
        calculateBalanceUser(baseModel, user, amount, walletId);

        return distribution;
    }

    /**
     * This function calculate the user's balance
     * @param baseModel
     * @param user
     * @param amount
     * @param walletId
     */
    public static void calculateBalanceUser(BaseModel baseModel, User user, int amount, int walletId) {

        // loop for found the user selected
        LoopUser : for (User userLocal : baseModel.getUsers()) {

            if (userLocal.getId() == user.getId()) {
                // loop for check if balance with this wallet is available
                for (Balance v : userLocal.getBalance()) {
                    if (v.getWalletId() == walletId) {
                        v.setAmount(v.getAmount() + amount);
                        break LoopUser;
                    }
                }
                // if this is not available, add the balance with wallet
                userLocal.getBalance().add(new Balance(walletId, amount));
                break;
            }
        }
    }

    /**
     * This function read the json file
     * @return
     * @throws IOException
     */
    public static BaseModel parseJson() throws IOException {

        Gson gson = new Gson();
        Reader reader = Files.newBufferedReader(Paths.get("src/main/java/backend/Level3/data/input.json"));
        BaseModel baseModel = gson.fromJson(reader, BaseModel.class);
        reader.close();

        return baseModel;
    }

    /**
     * This function write data to json file
     * @param baseModel
     * @throws IOException
     */
    public static void writeJson(BaseModel baseModel) throws IOException {

        Gson gson = new GsonBuilder().create();
        Writer writer = new FileWriter("src/main/java/backend/Level3/data/output.json");
        gson.toJson(baseModel, writer);
        writer.close();
    }

}
