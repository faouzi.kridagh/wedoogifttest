Pour s'authentifier utiliser la route :

    - GET : {{host}}/auth/company/{companyId}

    - En mettant l'id de la company (Mettre un companyId qui se trouve dans input.json), vous recevrez ensuite un token qui vous permettra d'être authentifié pour exécuter les fonctions de distributions, ce token est un JsonWebToken (JWT)

Route de test à utiliser avec le token : 

    - GET : {{host}}/test

    - Ajouter l'header pour chaque requête protégé, clé : Authorization, valeur : Bearer {token} 

Route pour effectuer une distribution à utliser avec le token :

    - POST : {{host}}/distributions/wallet/{walletId}

    - Request body : { "amount": 100, "userId": 1 } 

    - Renseignez le walletId (1 ou 2) pour spécifier le type de distributions (FOOD ou GIFT)