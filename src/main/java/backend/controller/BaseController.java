package backend.controller;

import backend.Main;
import backend.model.*;
import backend.security.config.JwtConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@RestController
public class BaseController {

    private final JwtConfig jwtConfig;

    @Autowired
    public BaseController(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }


    @GetMapping(path = "/test", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Map<String, String>> testEndpoint() {
        return ResponseEntity.ok(new HashMap<String, String>(){{
            put("test", "ok");
        }});
    }

    @PostMapping(path = "/distributions/wallet/{walletId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Distribution> distributionEndpoint(@AuthenticationPrincipal Company company, @PathVariable int walletId, @RequestBody Map<String, Integer> data) throws Exception {

        Distribution distribution = null;

        try {

            // read file in Level3/data/input.json
            BaseModel baseModel = Main.parseJson();

            int amount = data.get("amount");
            int userId = data.get("userId");

            User userSelected = baseModel.getUsers().stream().filter(v -> v.getId() == userId).findFirst().orElseThrow(() -> new Exception("User not found"));
            Company companySelected = baseModel.getCompanies().stream().filter(v -> v.getId() == company.getId()).findFirst().orElseThrow(() -> new Exception("Company not found"));

            distribution = Main.distributionFromCompany(baseModel, companySelected, userSelected, amount, walletId);

            // write file in Level3/data/output.json
            Main.writeJson(baseModel);

        } catch (Exception ex) {
            throw new Exception("Internal error");
        }
        return ResponseEntity.ok(distribution);
    }

    /**
     * Authentificate endpoints, enter company id
     * @param id this is id company
     * @return
     */
    @GetMapping(value = "/auth/company/{id}")
    public Map<String, String> auth(@PathVariable int id) {

        return Collections.singletonMap("token", jwtConfig.generateToken(new Company(id, "WedooTest", 1000)));
    }
}
