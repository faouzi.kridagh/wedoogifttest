package backend.model;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private int id;
    private List<Balance> balance;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Balance> getBalance() {
        return balance;
    }

    public void setBalance(List<Balance> balance) {
        this.balance = balance;
    }
}
