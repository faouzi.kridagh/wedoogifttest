package backend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Balance implements Serializable {

    @SerializedName("wallet_id")
    private int walletId;

    private Integer amount;

    public Balance() {
    }

    public Balance(int walletId, Integer amount) {
        this.walletId = walletId;
        this.amount = amount;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
