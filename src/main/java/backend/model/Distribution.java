package backend.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

public class Distribution implements Serializable {

    private int id;
    private Integer amount;

    @SerializedName("start_date")
    private String start;

    @SerializedName("end_date")
    private String end;

    @SerializedName("company_id")
    private Integer company;

    @SerializedName("user_id")
    private Integer user;

    @SerializedName("wallet_id")
    private Integer wallet;

    public Distribution() {
    }


    public Distribution(int id, Integer amount, int company, int user, int wallet) {
        this.id = id;
        this.amount = amount;

        if (wallet == 1) {
            this.start = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            this.end = LocalDate.now().plusYears(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } else {
            this.start = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            this.end = LocalDate.of(LocalDate.now().plusYears(1).getYear(), Month.FEBRUARY, 1).with(lastDayOfMonth()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }

        this.company = company;
        this.user = user;
        this.wallet = wallet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public Integer getWallet() {
        return wallet;
    }

    public void setWallet(Integer wallet) {
        this.wallet = wallet;
    }
}
