package backend.security.provider;

import backend.model.Company;
import backend.security.auth.UserAuthentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public final class TokenAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    private static Logger logger = LoggerFactory.getLogger(TokenAuthenticationProvider.class);


    @Override
    protected void additionalAuthenticationChecks(final UserDetails d, final UsernamePasswordAuthenticationToken auth) {
    }


    @Override
    protected UserDetails retrieveUser(final String username, final UsernamePasswordAuthenticationToken authentication) {

        UserAuthentication u = (UserAuthentication) authentication.getPrincipal();

        Company authenticatedUser = new Company(u.getId(), "WedooTest", 1000);
        authenticatedUser.setToken(authentication.getCredentials().toString());

        return authenticatedUser;
    }
}
