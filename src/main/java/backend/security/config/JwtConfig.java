package backend.security.config;

import backend.model.Company;
import com.google.gson.Gson;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;
import java.util.stream.Collectors;

@Configuration
public class JwtConfig {

    @Value("${jwt.header}")
    private String header;

    @Value("${jwt.prefix}")
    private String prefix;

    @Value("${jwt.expiration}")
    private int expiration;

    @Value("${jwt.secret}")
    private String secret;

    public String getHeader() {
        return header;
    }

    public String getPrefix() {
        return prefix + " ";
    }

    public int getExpiration() {
        return expiration;
    }

    public String getSecret() {
        return secret;
    }


    public String generateToken(Company company) {
        long now = System.currentTimeMillis();

        return Jwts.builder()
                .setSubject(new Gson().toJson(company))
                .claim("authorities", company.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + expiration))
                .signWith(Keys.hmacShaKeyFor(secret.getBytes()), SignatureAlgorithm.HS512)
                .compact();
    }
}
